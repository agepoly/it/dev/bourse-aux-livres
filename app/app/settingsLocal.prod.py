import os


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends." + os.environ.get("DB_ENGINE", "sqlite3"),
        "NAME": os.environ.get("DB_NAME", "db.sqlite3"),
        "USER": os.environ.get("DB_USER"),
        "HOST": os.getenv("DB_HOST"),
        "PORT": os.getenv("DB_PORT"),
        "PASSWORD": os.environ.get("DB_PASSWORD"),
    }
}

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = False

POLYBOOKEXCHANGE_EMAIL_MANAGERS = "contact@agepoly.ch"

ALLOWED_HOSTS = os.environ.get("HOST", "").split(":")

ENABLE_SENTRY = True
SENTRY_DSN = os.environ.get("SENTRY_DSN", "")
