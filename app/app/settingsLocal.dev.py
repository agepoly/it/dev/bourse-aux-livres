import os


# Change this if you want to use a different database than the default app/db.sqlite3
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends." + os.environ.get("DB_ENGINE", "sqlite3"),
        "NAME": os.environ.get("DB_NAME", "db.sqlite3"),
        "USER": os.environ.get("DB_USER"),
        "HOST": os.getenv("DB_HOST"),
        "PORT": os.getenv("DB_PORT"),
        "PASSWORD": os.environ.get("DB_PASSWORD"),
    }
}

SECRET_KEY = "YouShouldTypeSomeRandomTextHere"
POLYBOOKEXCHANGE_EMAIL_MANAGERS = "your.name@epfl.ch"

DEBUG = True

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

# Set this if you want to use Sentry in development
SENTRY_DSN = ""
ENABLE_SENTRY = False

# Write emails to files in mails/ rather than sending them for real
EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
EMAIL_FILE_PATH = os.path.join(BASE_DIR, "mails")
