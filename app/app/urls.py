from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path


import polybookexchange.urls
from app.tequila import login
from app.views import add_admin_autocomplete, add_superadmin_autocomplete, privilege_admin


urlpatterns = [
    # Examples:
    # url(r'^$', 'app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    path("i18n/", include("django.conf.urls.i18n")),
    path("", include(polybookexchange.urls)),
    path("login", login),
    path("", include("django.contrib.auth.urls")),
    path("admin/privilege", privilege_admin, name="privilege_admin"),
    path("admin/privilege/autocomplete/admin", add_admin_autocomplete, name="add_admin_autocomplete"),
    path("admin/privilege/autocomplete/superadmin", add_superadmin_autocomplete, name="add_superadmin_autocomplete"),
]

# Serve media files during development
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
