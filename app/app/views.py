from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import Http404, JsonResponse
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _


User = get_user_model()


@login_required
@staff_member_required
def privilege_admin(request):
    if not request.user.is_superuser:
        raise Http404()

    if request.method == "POST":
        if "action" not in request.POST:
            messages.error(request, _("No action specified."))

        else:
            action = request.POST["action"]

            if action in ("add_admin", "add_superadmin"):
                if "scipers" not in request.POST:
                    messages.error(request, _("No SCIPERs specified."))

                else:
                    scipers = request.POST.getlist("scipers")

                    users = User.objects.filter(username__in=scipers)

                    if action == "add_admin":
                        users.update(is_staff=True)
                    else:
                        users.update(is_staff=True, is_superuser=True)

                    messages.success(request, _("Success!"))

            elif action in ("promote", "demote", "remove"):
                if "sciper" not in request.POST:
                    messages.error(request, _("No SCIPER specified."))

                else:
                    sciper = request.POST["sciper"]

                    user = User.objects.filter(username=sciper)

                    if action == "promote":
                        user.update(is_superuser=True, is_staff=True)
                    elif action == "demote":
                        user.update(is_superuser=False)
                    else:
                        user.update(is_superuser=False, is_staff=False)

                    messages.success(request, _("Success!"))

            else:
                messages.error(request, _("Invalid action"))

        return redirect("privilege_admin")

    staff_members = User.objects.filter(is_staff=True, is_superuser=False).order_by("username")
    superusers = User.objects.filter(is_superuser=True).order_by("username")

    return render(request, "app/privilege_admin.html", {"staff_members": staff_members, "superusers": superusers})


@login_required
@staff_member_required
def add_admin_autocomplete(request):
    if not request.user.is_superuser:
        raise Http404()

    if "q" not in request.GET:
        return JsonResponse({"results": []})

    results = User.objects.filter(is_staff=False)

    for term in request.GET["q"].strip().split():
        results = results.filter(Q(username=term) | Q(first_name__icontains=term) | Q(last_name__icontains=term))

    return JsonResponse({"results": [{"id": user.username, "text": user.get_full_name()} for user in results][:10]})


@login_required
@staff_member_required
def add_superadmin_autocomplete(request):
    if not request.user.is_superuser:
        raise Http404()

    if "q" not in request.GET:
        return JsonResponse({"results": []})

    results = User.objects.filter(is_superuser=False)

    for term in request.GET["q"].strip().split():
        results = results.filter(Q(username=term) | Q(first_name__icontains=term) | Q(last_name__icontains=term))

    return JsonResponse({"results": [{"id": user.username, "text": user.get_full_name()} for user in results[:10]]})
