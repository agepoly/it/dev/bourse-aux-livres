��          �   %   �      @     A     E  _   T     �     �     �  7   �  
        $  	   3     =     R     h     }  ,   �  )   �     �  %        )  2   0     c     j     s       !  �     �     �  �   �     A     N     V  A   p     �     �     �     �     �             <   3  8   p     �  /   �  	   �  A   �     ;  	   B     L     X                                 
                                                                                 	            Add Administrators Administrators can access the administration pages except this one. Superadmins can do anything Are you sure? Cancel Demote to administrator Demote user ${sciper} from superadmin to administrator? First name Invalid action Last name No SCIPER specified. No SCIPERs specified. No action specified. Privilege administration Promote the selected users to administrator? Promote the selected users to superadmin? Promote to superadmin Promote user ${sciper} to superadmin? Remove Remove the administrator status of user ${sciper}? SCIPER Success! Superadmins Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-13 20:40+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
 Ajouter Administrateurs Les administrateurs peuvent accéder aux pages d'administration sauf celle-ci. Les superadmins peuvent accéder à toutes les pages Es-tu sûr ? Annuler Changer en administrateur Changer l'utilisateur ${sciper} de superadmin à administrateur ? Prénom Action invalide Nom Aucun SCIPER spécifié. Aucun SCIPER spécifié. Aucune action spécifiée. Gestion des privilèges Promouvoir les utilisateurs sélectionnés administrateurs ? Promouvoir les utilisateurs sélectionnés superadmins ? Promouvoir superadmin Promouvoir l'utilisateur ${sciper} superadmin ? Supprimer Révoquer le statut d'administrateur de l'utilisateur ${sciper} ? SCIPER Succès ! Superadmins Oui 