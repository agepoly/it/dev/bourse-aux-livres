# Bourse aux Livres

Small wrapper around polybookexchange

## Installation
- Clone the repository
- Install the dependencies with `pip install -r pip-reqs.txt`. You'll probably want to do that in a virtualenv instead. If you don't have a virtualenv manager of choice, I suggest [pyenv](https://github.com/pyenv/pyenv). You'll also probably want to install a local copy of polybookexchange (`pip -e .` from the folder where you cloned polybookexchange) rather than from Github
- Copy `app/app/settingsLocal.dev.py` to `app/app/settingsLocal.py`. The latter file is gitignored. You should look at its content and edit fields to match your preferences
- `cd app`
- Load the initial data with `python manage.py loaddata polybookexchange_epfl.yaml`
- Run the development server with `python manage.py runserver`
- Connect to http://localhost:8000
- Login to tequila. When coming back from tequila you'll see an error. Just change the https in the address bar to http
- You'll probably want to grant yourself staff status. To do this, start an interactive interpreter with `python manage.py shell`. In the shell, run the following:
  ```
  >>> from django.contrib.auth import get_user_model
  >>> User = get_user_model()
  >>> u = User.objects.get(username="253797")
  >>> u.is_staff = True
  >>> u.save()
  ```
