FROM python:3.9

RUN apt update && apt install -y \
	sqlite3 \
    libsasl2-dev \
    libldap2-dev \
    default-mysql-client \
    default-libmysqlclient-dev \
    python3-ldap \
    postgresql-client \
    && apt clean

COPY . .

RUN pip install -U pip && pip install -Ur pip-reqs.txt && pip install -U uwsgi

ARG CI_COMMIT_SHA
ENV COMMIT_SHA=$CI_COMMIT_SHA

WORKDIR /app

RUN cp app/settingsLocal.prod.py app/settingsLocal.py

RUN SECRET_KEY=fake python manage.py collectstatic --noinput

CMD (python manage.py migrate --fake-initial --noinput || true ) && \
    python manage.py migrate --noinput && \
    uwsgi --http 0.0.0.0:8080 --enable-threads --static-map /static=/app/static --static-map /media=/app/media  --master --processes=5 --harakiri=20 --max-requests=5000 --env DJANGO_SETTINGS_MODULE=app.settings --module=app.wsgi:application --log-master --log-format '%(addr) - %(user) [%(ltime)] "%(method) %(uri) %(proto)" %(status) %(size) "%(referer)" "%(uagent)"' 2>/dev/null
